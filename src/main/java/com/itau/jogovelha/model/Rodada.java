package com.itau.jogovelha.model;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class Rodada {
	private Jogador[] jogadores = new Jogador[2];
	private Jogo jogo;

	public Rodada(String nomeUm, String nomeDois){


		jogadores[0] = new Jogador(nomeUm);
		jogadores[1] = new Jogador(nomeDois);

		jogo = new Jogo();
		//jogoServicoPost(jogo, "criar");
		}

	public void iniciarJogo(){
		jogo = jogoServicoGet();
		if(! jogo.isEncerrado()){
			return;
		}

		jogo = new Jogo();
		//jogoServicoPost(jogo, "criar");
	}

	public void jogar(int x, int y){
		jogo = jogoServicoGet();
		if(jogo.isEncerrado()){
			return;
		}

		//jogo.jogar(x, y);
		Jogada jogada = new Jogada();
		jogada.x = x;
		jogada.y = y;
		jogoServicoPost(jogada, "jogar");
		
		if(jogo.isEncerrado() && jogo.isVitoria()){
			int vencedor = jogo.getJogadorAtivo();

			jogadores[vencedor].incrementarPontos();
		}
	}

	public Placar getPlacar(){
		int[] pontuacao = {jogadores[0].getPontos(), jogadores[1].getPontos()};

		Placar placar = new Placar();
		placar.pontuacao = pontuacao;
		placar.casas = jogo.getCasas();
		placar.encerrado = jogo.isEncerrado();

		return placar;
	}

	public void jogoServicoPost(Jogada jogada, String func) {
		try {
			RestTemplate restTemplate = new RestTemplate();

			Jogo jogoRes = restTemplate.postForObject("http://localhost:8080/jogo/" + func, jogada, Jogo.class);
			jogo = jogoRes;
		} 		
		catch (HttpClientErrorException e) {

			System.out.println(e.getResponseBodyAsString());
		}
		catch(Exception e)

		{
			System.out.println(e.getMessage());
		}
	}
	public Jogo jogoServicoGet() {
		try {
			RestTemplate restTemplate = new RestTemplate();

			Jogo jogoRes = restTemplate.getForObject("http://localhost:8080/jogo", Jogo.class);
			return jogoRes;
		} 		
		catch (HttpClientErrorException e) {

			System.out.println(e.getResponseBodyAsString());
			return null;
		}
		catch(Exception e)

		{
			System.out.println(e.getMessage());
			return null;
		}
	}

}
